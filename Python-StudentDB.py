# This script facilitates entry of student data into a database as well as a way to retrieve information from the database.
# Created by Kevin Clark.
import sqlite3


def ReadDatabase(): # Function to search the database for one or multiple records.
	print "What field would you like to search by?" 
	print "1. Student ID." # User choice visual display.
	print "2. Student name."
	print "3. Student enrollment date."
	print "4. Student graduation date."
	UserFieldChoice = raw_input()
	
	if UserFieldChoice == "1": # User Choice logic.
		SearchField = "student_id"
	elif UserFieldChoice == "2":
		SearchField = "student_name"
	elif UserFieldChoice == "3":
		SearchField = "enroll_date"
	elif UserFieldChoice == "4":
		SearchField = "grad_date"
	else:
		print "You have entered an invalid choice.  Program is exiting."
		exit()
	
	FieldValue = raw_input("What is the value of "+SearchField+" that you want to look for?\r\n")
	RecordList = CursorObject.execute("SELECT * FROM students WHERE "+SearchField+"='"+FieldValue+"';").fetchall() # Line that actually searches the database for matching records.
	RecordString = str(RecordList)
	if RecordString == "[]": # If the returned record is "[]", then there were no matches found.
		print "No matching records were found."
	else: # If there was a matched record found,
		print RecordList # Print it to the screen.

	
def WriteDatabase(): # Function to write a record to the database.
	try: # Creates the students sqlite table unless it already exists.
		CursorObject.execute("CREATE TABLE students ( student_id INTEGER PRIMARY KEY AUTOINCREMENT, student_name VARCHAR, enroll_date VARCHAR , grad_date VARCHAR );")
	except: # If it already exists...
		pass # do nothing.
	
	student_name = raw_input("What student would you like to add to the database?\r\n") # Gather user input data for the student name,
	student_id = raw_input("Enter the student id for "+student_name+".\r\n") #  student id,
	enroll_date = raw_input("Enter the enrollment date in mm/dd/yyyy format for "+student_name+".\r\n") # enroll date,
	grad_date = raw_input("Enter the graduation date in mm/dd/yyyy format for "+student_name+".\r\n") # and graduation date.
	
	try:
		CursorObject.execute("INSERT INTO students VALUES ("+student_id+", '"+student_name+"', '"+enroll_date+"', '"+grad_date+"' );") # Add the record containing student info.
	except:
		print "Error entering the Student ID.  Student ID must be a unique integer.  Exiting."
		exit()


DBPath = raw_input("Provide the path of a new or existing student database. (Use double backslashes if neccesary):\r\n") # User input for the location of the database.
try:
	Database = sqlite3.connect(DBPath) # Establish the database connection...
	CursorObject = Database.cursor() # and cursor object needed to read and write to the database.
except: # Error handling for invalid filenames and paths of the database.
	print "Encountered an error with the filename.  Exiting."
	exit()


print "What would you like to do to the database?" # Visual choice display for the user.
print "1. Read a record from the database."
print "2. Write a record to the database."
print "3. Exit this program."
DBAction = raw_input()

if DBAction == "1": # Logic that uses the user's choice.
	ReadDatabase()
elif DBAction == "2":
	WriteDatabase()
elif DBAction == "3":
	exit()
else: # If the user can't enter a choice properly, this will catch the error.
	print "You have entered an invalid choice.  Program is exiting."
	exit()


Database.commit() # Save...
Database.close() # and quit the database connection.
